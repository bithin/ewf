#!/bin/bash/python
"""

Title: Blogger Crawler

Author: Bithin A 

Email ID: bithin2007@gmail.com


"""

import random
import re
import subprocess

import time

from blogger_crawler import get_blog_id
from blogger import blogger_crawl, google


def crawl():

    keyword = ["almighty allah", "babri masjid", "bal thackery", "best bakery case india", "china attacks india", "hate india", "hindu muslim", "india jihad", "indian mujahideen", "islam terrorism", "Lashkar-e-Taiba", "Lashkar-e-Tayyaba", "Nagaland-Isak-Muivah", "prayers for ajmal kasab", "students islamic movement of india"]    # Add keywords here
            
    sleep_times = [6,5,7,3,9]

    for word in keyword:
        blog_list = []
        clean_blog_list = []
        blog_list = google(word)
        print "Search with keyword "+word
        clean_blog_list = clean_blog_url(list(set((blog_list))))
        archieve_crawl(clean_blog_list, word)
        sleep_time = sleep_times[random.randint(0, len(sleep_times) - 1)]
        time.sleep(sleep_time * 60)

def clean_blog_url(blog_list):
    
    """ 
    Clean the multiple blog urls

    """ 
    clean_list = []
    for item in blog_list:
        clean_list.append("http://"+item)

    return clean_list

def archieve_crawl(blog_list, word):

    for blog in blog_list:
        print "Crawling "+blog+" ....."
        blog_id = get_blog_id(blog)
        blogger_crawl(blog_id, word, blog)            


if __name__ == "__main__":
    """
    Starting the crawl

    """
    crawl()
