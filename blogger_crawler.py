import urllib2
import re
import time 
import random
import os

from BeautifulSoup import BeautifulSoup

import socks
import socksipyhandler

import xml.etree.cElementTree as ET

def get_blog_id(url): 
    
    source = ''
    posts = []
    page = crawl_page(url)
    soup = BeautifulSoup(page)
    soup.prettify()
    
    blog_id = 0
    post_link = soup.findAll('link', href=True)
    b_start = re.compile("/feeds/")
    b_end = re.compile("/posts/")
    if post_link:
        for anchor in post_link:
            if b_start.search(anchor['href']) is not None and b_end.search(anchor['href']) is not None:
                start = b_start.search(anchor['href']).end() 
                end = b_end.search(anchor['href']).start() 
                blog_id = anchor['href'][start:end]  

    return blog_id
    
    
def crawl_page(page):
    opener = urllib2.build_opener(socksipyhandler.SocksiPyHandler(socks.PROXY_TYPE_SOCKS4, 'localhost', 9050))
    opener.addheaders = [('User-Agent', ('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:17.0) Gecko/20100101 Firefox/17.0'))]
    response = opener.open(page)
    return response.read()


