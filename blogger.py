#!/usr/bin/python2.7
import gflags
import httplib2
import logging
import pprint
import sys
import os

from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run

from BeautifulSoup import BeautifulSoup

import xml.etree.cElementTree as ET

FLAGS = gflags.FLAGS

CLIENT_SECRETS = 'client_secrets.json'


MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

%s

with information from the APIs Console <https://code.google.com/apis/console>.

""" % os.path.join(os.path.dirname(__file__), CLIENT_SECRETS)


FLOW = flow_from_clientsecrets(CLIENT_SECRETS,
           scope='https://www.googleapis.com/auth/blogger',
           message=MISSING_CLIENT_SECRETS_MESSAGE)


gflags.DEFINE_enum('logging_level', 'ERROR',
    ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    'Set the level of logging detail.')


def blogger_crawl(blog_id, word, blog_url):

    http = authorization()
    service = build("blogger", "v2", http=http)

    try:
        users = service.users()
        thisuser = users.get(userId="self").execute(http=http)
        thisusersblogs = users.blogs().list(userId="self").execute()

        posts = service.posts()
        comments = service.comments()
        blogger = ET.Element("blogger")
        blogs = ET.SubElement(blogger, "blog")
        blogs.set("url", blog_url)

        contents = ''
        request = posts.list(blogId=blog_id, maxResults=20)
        
        post_doc = request.execute(http=http)

        
        posts_doc = request.execute(http=http)
        if 'items' in posts_doc and not (posts_doc['items'] is None):
            for post in posts_doc['items']:
                post_tag = ET.SubElement(blogs, "post")
                title = ET.SubElement(post_tag, "title")
                title.text = post['title']
                content_tag = ET.SubElement(post_tag, "content")
                content_tag.text = filterhtml(post['content'])
                comment_request = comments.list(blogId=blog_id, postId=post['id'])
                comment_doc = comment_request.execute(http=http)

                if 'items' in comment_doc and not ( comment_doc['items'] is None):
                    comment_tag = ET.SubElement(post_tag, "comments")
                    for comment in comment_doc['items']:
                        c_root = ET.SubElement(comment_tag, "comment")
                        c_author = ET.SubElement(c_root, "author")
                        c_author.text = comment['author']['displayName']
                        c_content = ET.SubElement(c_root, "content")
                        c_content.text = filterhtml(comment['content'])
                        c_date = ET.SubElement(c_root, "Publish Date")
                        c_date.text = comment['published']
                           

            tree = ET.ElementTree(blogger)
            tree.write('results/'+word+'_'+blog_id+'.xml')
            print "Done"
    
    except AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-runthe application to re-authorize")


def google(keyword):

    http = authorization()
    service = build("customsearch", "v1", developerKey="AIzaSyBmhzvfIIAXKndUF_E49wNJr8Sh1EKY30s")
    
    google_result = service.cse().list(q=keyword, cx="011421993024149555845:8iuxkwemifk", gl='in').execute()

    search_result = []
    if 'items' in google_result and not (google_result['items'] is None):
        for url in google_result['items']:
            search_result.append(url['displayLink'])

    return search_result

def filterhtml(content):
    soup = BeautifulSoup(content)
    soup.prettify()
    contents = ''
    for tmp in soup.findAll(text=True):
        contents = contents + "\n" + tmp

    return contents

def authorization():
    storage = Storage('blogger.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        credentials = run(FLOW, storage)

    http = httplib2.Http()
    http = credentials.authorize(http)
    return http 
